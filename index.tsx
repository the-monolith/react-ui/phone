import { ErrorMessage } from 'local/react-ui/error-message'
import { Tooltip } from 'local/react-ui/tooltip'
import { component, React, Style } from 'local/react/component'

export const defaultPhoneStyle: Style = {
  color: 'inherit',
  textDecoration: 'none',
  position: 'relative',
  display: 'inline-block'
}

export const defaultCountryCode = '1'

export const defaultTooltipText = 'Click to call'

export const Phone = component
  .props<{
    countryCode?: string
    style?: Style
    number: string
    tooltipText?: string
  }>({
    countryCode: defaultCountryCode,
    style: defaultPhoneStyle,
    tooltipText: defaultTooltipText
  })
  .render(({ countryCode, number, style, tooltipText }) =>
    number.length === 10 ? (
      <a href={`tel:+${countryCode}${number}`} style={style}>
        <Tooltip text={tooltipText} />
        {countryCode ? `+${countryCode} ` : null}
        {number.substr(0, 3)} {number.substr(3, 3)}-{number.substr(6)}
      </a>
    ) : (
      <ErrorMessage message="Invalid phone number (should be ##########)" />
    )
  )
